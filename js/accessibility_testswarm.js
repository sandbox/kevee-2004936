(function ($) {
  Drupal.behaviors.a11y_testing_test = {
    
    guideline : {},
    
    tests     : {},
    
    quailGuideline : [ ],
    
    alreadyRun : false,
    
    failedItems : { },
    
    attach: function (context) {
    	if(this.alreadyRun) {
    		return;
    	}
    	this.alreadyRun = true;
    	var that = this;

    	that.quailGuideline = [];
    	$.ajax({ url : Drupal.settings.basePath + 'accessibility_testswarm/guideline.json',
  				 					async : false,
  				 					dataType : 'json',
  				 					success : function(data) {
				  				 	  that.guideline = data;
				  				 	  $.each(that.guideline, function(testName, test) {
				  				 	  	that.quailGuideline.push(testName);
				  				 	  });
				  				  }
				  				 });
			
  		$.ajax({ url : Drupal.settings.basePath + Drupal.settings.accessibility_testswarm.path + '/js/quail/src/resources/tests.json',
  				 					async : false,
  				 					dataType : 'json',
  				 					success : function(data) {
				  				 	  that.tests = data;
				  				  }
				  				 });
  		module(Drupal.t('Accessibility tests'), {
			        setup    : function() { },
			        teardown : function() { }
			      });

  		$('body').quail({ accessibilityTests : that.tests,
														guideline : that.quailGuideline,
														jsonPath : Drupal.settings.basePath + Drupal.settings.accessibility_testswarm.path + '/js/quail/src/resources',
														complete : function(event) {
															$.each(event.results, function(testName, items) {
                                if(items.length) {
                                  for(var i = 0; i < items.length; i++) {
                                    if((items[i].attr('id') && items[i].attr('id').search('qunit-') === 0) || items[i].hasClass('-a11y-testswarm')) {
                                      items.splice(i, 1);
                                      i--;
                                    }
                                  }
                                }
                                test(that.guideline[testName], function() {
													        expect(1);
													        if(items.length) {
													        	that.failedItems[testName] = items;
													        }
													        equal(0, items.length, testName);
													      });
															});
													}});
    }
    
  };
  
  Drupal.behaviors.testswarm.attach = function() {
    var currentTest;
    var mySettings = drupalSettings.testswarm;

    $.extend(QUnit.config, {
      reorder: false, // Never ever re-order tests!
      altertitle: false, // Don't change the title
      autostart: false
    });

    var logger = {log: {}, info: {}, tests: []};
    var currentModule = 'default';

    QUnit.moduleStart = function(module) {
      currentModule = module.name;
      if (!logger.log[currentModule]) {
        logger.log[currentModule] = {};
      }
    };

    QUnit.testStart = function(test) {
      currentTest = test.name;
    };

    QUnit.testDone = function(test) {
      logger.tests.push(test);
    };

    QUnit.done = function(data) {
      logger.info = data;
      logger.caller = mySettings.caller;
      logger.theme = mySettings.theme;
      logger.token = mySettings.token;
      logger.karma = mySettings.karma;
      logger.module = mySettings.module;
      logger.description = mySettings.description;
			$.each(logger.log[Drupal.t('Accessibility tests')], function(index, log) {
				$.each(log, function(logIndex, item) {
					if(item.result) {
						item.message = Drupal.t('No errors found');
					}
					else {
						item.accessibility_testswarm = [];
						$.each(Drupal.behaviors.a11y_testing_test.failedItems[item.message], function(e, element) {
              var theme = (element.parents('.-a11y-testswarm').length)
                          ? Drupal.settings.accessibility_testswarm_theme[element.parents('.-a11y-testswarm').first().data('theme-key')]
                          : '<none>';
							item.accessibility_testswarm.push({ element : $('<div>').append(element.clone().empty()).html(),
                                                  theme : theme});
						});
					}
				});
			});
			// Write back to server
      var url = Drupal.url('testswarm-test-done');
      jQuery.ajax({
        type: "POST",
        url: url,
        timeout: 10000,
        data: logger,
        error: function(response) {
          window.alert(Drupal.ajaxError(response, url));
        },
        success: function(){
          window.setTimeout(function() {
            if (!mySettings.debug || mySettings.debug !== 'on') {
              if (mySettings.destination) {
                window.location = mySettings.destination;
              }
              else {
                window.location = '/testswarm-browser-tests';
              }
            }
          }, 500);

        }
      });
    };
    QUnit.log = function(data) {
      if (!logger.log[currentModule]) {
        logger.log[currentModule] = {};
      }
      if (!logger.log[currentModule][currentTest]) {
        logger.log[currentModule][currentTest] = [];
      }
      logger.log[currentModule][currentTest].push(data);
    };
  };
})(jQuery);