<?php

namespace Drupal\accessibility_testswarm;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerInterface;
use Drupal\Core\Config;
use Symfony\Component\HttpFoundation\JsonResponse;

class RetrieveAccessibilityGuidelineController implements ControllerInterface {

  public function __construct() {
  }

  /**
   * Returns new instance
   */
  public static function create(ContainerInterface $container) {
    return new RetrieveAccessibilityGuidelineController();
  }
  /**
   * Returns a list of QUAIL tests to run
   *
   * @return
   *   JSON array of tests to call
   */
  public function getGuideline() {
  	$config = config('accessibility_testswarm.settings');
	  return new JsonResponse(($config) ? $config->get('guideline') : array());
  }

}